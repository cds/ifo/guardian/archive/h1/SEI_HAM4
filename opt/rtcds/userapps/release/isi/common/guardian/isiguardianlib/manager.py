"""
SEI chamber manager library

for common functions/states/decorators/etc. between HAM and BSC
chamber managers

"""
from guardian import GuardState, GuardStateDecorator

from . import const as top_const
from .masterswitch import const as ms_const
from .watchdog import util as wd_util

import cdsutils
import numpy as np
import time

##################################################

if top_const.CHAMBER[:3] == 'HAM':
    ISOLATED_STATE = {'HPI': 'ROBUST_ISOLATED',
                      'ISI': 'HIGH_ISOLATED'}
else:
    ISOLATED_STATE = {'HPI': 'ROBUST_ISOLATED',
                      'ST1': 'HIGH_ISOLATED',
                      'ST2': 'HIGH_ISOLATED'}

##################################################

def is_hpi_masterswitch_on():
    return ezca['HPI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['HPI']] == ms_const.MASTERSWITCH_ON


def is_isi_masterswitch_on():
    return ezca['ISI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']] == ms_const.MASTERSWITCH_ON


def masterswitches_on(nodes):
    for node in nodes:
        if node.name.split('_')[0] == 'ISI':
            ezca['ISI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']] = ms_const.MASTERSWITCH_ON
        elif node.name.split('_')[0] == 'HPI':
            ezca['HPI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['HPI']] = ms_const.MASTERSWITCH_ON


def masterswitches_off(nodes):
    for node in nodes:
        if node.name.split('_')[0] == 'ISI':
            ezca['ISI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']] = ms_const.MASTERSWITCH_OFF
        elif node.name.split('_')[0] == 'HPI':
            ezca['HPI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['HPI']] = ms_const.MASTERSWITCH_OFF

############################

def coilmon_ok():
    if 'HAM' in top_const.CHAMBER:
        return ezca['ISI-' + top_const.CHAMBER + '_' + 'BIO_IN_COILMON_STATUS_ALL_OK'] == ms_const.MASTERSWITCH_ON
    else:
        return ezca['ISI-' + top_const.CHAMBER + '_' + 'COILMON_STATUS_ALL_OK'] == ms_const.MASTERSWITCH_ON
#############################

def node_watchdog_tripped(node):
    return 'WATCHDOG_TRIPPED' in node.state


def watchdog_tripped_nodes(nodes):
    """Return list of nodes that are in tripped state."""
    tripped_nodes = []
    for node in nodes:
        if node_watchdog_tripped(node):
            tripped_nodes.append(node)
    return tripped_nodes

##################################################

# Added the node arg here to allow this decorator to be used with the HAM7 & 8
# manager node (isi_manager.py).
def get_masterswtich_check_docorator(nodes):
    class masterswitch_check(GuardStateDecorator):
        """Check that the master switch has not been turned off."""
        def pre_exec(self):
            off = []
            for node in nodes:
                if node.name.split('_')[0] == 'HPI':
                    if not is_hpi_masterswitch_on():
                        off.append('HPI')
                else:
                    if not is_isi_masterswitch_on():
                        off.append('ISI')
            if off:
                notify("%s masterswitch off" % (', '.join(off)))
                return 'INIT'
    return masterswitch_check


class coilmon_check(GuardStateDecorator):
    """Check ISI coilmon status. Only checks ok bit, which trips after any coil overtemp has been tripped 60sec"""
    def pre_exec(self):
        off = []
        if not coilmon_ok():
            off.append('ISI')
        if off:
            notify("%s coilmon not ok" % (', '.join(off)))
            #return 'INIT'


class watchdog_dackill_check(GuardStateDecorator):
    """Check that the DACKILL watchdog has not tripped."""
    def pre_exec(self):
        return None
        if wd_util.is_dackill_tripped():
            return 'WATCHDOG_TRIPPED_DACKILL'


def get_subordinate_watchdog_check_decorator(nodes):
    class subordinate_watchdog_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                return 'WATCHDOG_TRIPPED_SUBORDINATE'
    return subordinate_watchdog_check

##################################################

def get_watchdog_tripped_subordinate_state(nodes):
    class WATCHDOG_TRIPPED_SUBORDINATE(GuardState):
        request = False
        index = 12

        @watchdog_dackill_check

        def main(self):        
            if top_const.CHAMBER == 'HAM6' and ezca.prefix == 'L1:':      #L1 and HAM6 flag         
                t0 = ezca['ISI-HAM6_WD_MON_GPS_TIME']+1
                time.sleep(1)
                check1 = True   # check1,2,3 are to keep it in the same trip stage if getdata/numpy fails
                check2 = False
                check3 = False
                try:
                    wd_state = cdsutils.getdata('ISI-HAM6_WD_MON_STATE_INMON',5*60,t0-5*60-5) #data for WD state for last ~5 mins
                    fash_state = cdsutils.getdata('SYS-MOTION_C_FASTSHUTTER_A_STATE',5,t0-5) #fastshutter state within ~5s before trip
                    check1 = np.mean(wd_state.data)!=1
                    check2 = np.mean(fash_state.data)!=0
                    check3 = np.mean(fash_state.data)!=1
                except:
                    return False
                else:
                    if check1: #checking if there is any trip last 5 mins. 
                        log('HAM6 tripped second time within 5 mins. Need more investigation. Check fastshutter and else.\n')            
                        return False
                    elif watchdog_tripped_nodes(nodes) and (check2 or check3): #check if WDtrip due to fastshutter
                        log('Fastshutter tripped HAM6. Will wait 30s and then proceed to untrip the WD\n')
                        time.sleep(30)
                        ezca['ISI-HAM6_WD_RSET']=1
                        while 'WATCHDOG_TRIPPED' in nodes['ISI_HAM6'].state: #wait until ISI node catches up to clear all of its WATCHDOG_TRIPPED stages 
                            continue
                        return True
                    else:
                        return False
            return False

        def run(self):
            notify('WATCHDOG TRIP: SUBORDINATE')
            if watchdog_tripped_nodes(nodes):
                return False
            return True

    return WATCHDOG_TRIPPED_SUBORDINATE


def get_offline_state(nodes):
    class OFFLINE(GuardState):
        request = True
        index = 25

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @nodes.checker()
        def main(self):
            masterswitches_off(nodes)

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @nodes.checker()
        def run(self):
            for stalled_node in nodes.get_stalled_nodes():
                log("%s is stalled (request=%s, state=%s)" % (stalled_node.name, stalled_node.request, stalled_node.state))
                stalled_node.revive()
            return nodes.arrived

    return OFFLINE


def get_ready_state(nodes):
    class READY(GuardState):
        request = True
        index = 30

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @nodes.checker()
        def main(self):
            masterswitches_on(nodes)
            for node in nodes:
                node.set_request('READY')

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @get_masterswtich_check_docorator(nodes)
        @coilmon_check
        @nodes.checker()
        def run(self):
            for stalled_node in nodes.get_stalled_nodes():
                log("%s is stalled (request=%s, state=%s)" % (stalled_node.name, stalled_node.request, stalled_node.state))
                stalled_node.revive()
            return nodes.arrived

    return READY


def get_idle_state(nodes, requestable=True):
    class IdleState(GuardState):
        request = requestable

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @get_masterswtich_check_docorator(nodes)
        @coilmon_check
        @nodes.checker()
        def run(self):
            if not nodes.arrived:
                notify("nodes have changed state")
                return 'INIT'
            return True

    return IdleState


def get_move_control_loops_state(nodes, stage, requested_state):
    class MoveControlLoops(GuardState):
        request = False

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @get_masterswtich_check_docorator(nodes)
        @coilmon_check
        @nodes.checker()
        def main(self):
            nodes[stage] = requested_state

        @get_subordinate_watchdog_check_decorator(nodes)
        @watchdog_dackill_check
        @get_masterswtich_check_docorator(nodes)
        @coilmon_check
        @nodes.checker()
        def run(self):
            for stalled_node in nodes.get_stalled_nodes():
                log("%s stalled (request=%s, state=%s)" % (stalled_node.name, stalled_node.request, stalled_node.state))
                stalled_node.revive()
            return nodes.arrived

    return MoveControlLoops

##################################################

class WATCHDOG_TRIPPED_DACKILL(GuardState):
    request = False
    index = 14

    def run(self):
        notify('WATCHDOG TRIP: DACKILL')
        if wd_util.is_dackill_tripped():
            return False
        return True

class ISI_COILMON_TRIPPED(GuardState):
    request = False
    index = 15

    def run(self):
        notify('ISI coilmon not ok')
        if not coilmon_ok():
            return False
        return True
